# coding: UTF-8

from loading_data import LoadData
from predictor import TransformPredictor
import json

def predictor_1(data, is_production=False, stock_name=None):
	columns_to_standardize = []
	predictor = TransformPredictor(
		columns_to_standardize=columns_to_standardize,
		data=data)
	predictor.compile_model()
	if is_production:
		print("PRODUCCION \n")
		# predictor.fit_model(epochs=90)
		# save_model(predictor, 'predictor_1_' + stock_name)
	else:
		print("TESTING \n")
		predictor.test_model(n_splits=9, epochs=100, verbose=0)
		# print_data('predictor_1',predictor.train_results,predictor.test_results)

def run_tests(stocks_data):
	for stock in stocks_data:
		print (" ************************* " + stock + " ******************************\n")
		predictor_1(stocks_data[stock])
        
def main():
	"""Método exclusivo para pruebas locales de funcionamiento."""
	isTesting = True
	loadData = LoadData()
	stocks_data = loadData.beginLoadData()
	if isTesting:
		run_tests(stocks_data)
	else:
		# train_production_models(stocks_data)
		print("*objects, sep=' ', end='\n', file=sys.stdout")




if __name__ == '__main__':
	main()

