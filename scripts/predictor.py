# coding: UTF-8

import numpy as np
import pandas as pd

# Import `StandardScaler` from `sklearn.preprocessing`
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_score, recall_score, f1_score, cohen_kappa_score


# Se usa Keras como la librería para manejar
# las redes neuronales.
from keras.models import Model
# Import `Sequential` from `keras.models`
from keras.models import Sequential
from keras.layers import Input, Dense
from keras.callbacks import ModelCheckpoint


class TransformPredictor(object):
	"""docstring for TransformPredict"""
	def __init__(self, dataset_path=None,columns_to_standardize=None,data=None):
		self.dataset_path = dataset_path
		self.columns_to_standardize = columns_to_standardize
		self.data = data
		print("BEGIN PREDICTOR")
		self.__preprocess_data()
		self.__create_model()

	def __preprocess_data(self):
		"""Preprocesamiento del conjunto de datos."""
		if (self.data is None):
			print("NO HAY DATOS, Debe cargar el .csv")
		else:
			print("HAY DATOS EN EL DATAFRAME")
			print("DATAS-> ", self.data)

	def __create_model(self):
		# Specify the data 
		X=self.data.ix[:,0:8]
		
		# Specify the target labels and flatten the array 
		y=np.ravel(self.data.TYPE)
		# Split the data up in train and test sets
		self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=0.33, random_state=42)

		# Define the scaler 
		scaler = StandardScaler().fit(self.X_train)
		# Scale the train set
		self.X_train = scaler.transform(self.X_train)
		# Scale the test set
		self.X_test = scaler.transform(self.X_test)
		print("X-> \n", self.X_train)
		# print("X-> ", self.X_train)
		# Initialize the constructor
		self.model = Sequential()
		# Add an input layer 
		self.model.add(Dense(12, activation='relu', input_shape=(8,)))

		# Add one hidden layer 
		self.model.add(Dense(8, activation='relu'))
		self.model.add(Dense(8, activation='relu'))
		self.model.add(Dense(8, activation='relu'))


		# Add an output layer 
		self.model.add(Dense(1, activation='sigmoid'))

			
			
	def compile_model(self):
		metrics = ['mean_squared_error', 'mean_absolute_error', 'mean_absolute_percentage_error']
		self.model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
		#self.model.compile(loss='mean_squared_error', optimizer='adam', metrics=metrics)

	def test_model(self, n_splits=9, cv_runs=10, epochs=100, verbose=2):
		"""Evaluación del modelo usando validación cruzada
		hacia adelante."""
		print(" objects  file=sys.stdout")
		self.points=ModelCheckpoint(
					'./saved_models/weights.h5',
					monitor='val_acc',
					save_best_only=True,
					mode="max",
					save_weights_only=True,
					verbose=1)

		self.model.fit(self.X_train,self.y_train,epochs=epochs, batch_size=1, verbose=verbose,
 			validation_data=(self.X_test,self.y_test),callbacks=[self.points])
		self.model.load_weights('white.h5')
		self.y_pred = self.model.predict(self.X_test)


		for i in range(len(self.y_pred)):
			self.y_pred[i][0]=round(self.y_pred[i][0])

		self.y_pred=np.array(self.y_pred).astype(np.int32)

		print (self.y_pred[:5])
		print (self.y_test[:5])

		self.score = self.model.evaluate(self.X_test, self.y_test,verbose=verbose)

		print("SCORE -> ", self.score)

		# Confusion matrix
		# confusion_matrix(y_test, y_pred)
		print(confusion_matrix(self.y_test, self.y_pred))
		# Presicion
		print(precision_score(self.y_test, self.y_pred))


		# Recall
		print(recall_score(self.y_test, self.y_pred))
		# F1 score
		print( f1_score(self.y_test,self.y_pred))

		# Cohen's kappa

		print(cohen_kappa_score(self.y_test, self.y_pred))
		



def test():
	"""Método exclusivo para pruebas locales de funcionamiento."""
	dataset_path = "datasets/dataproduction.csv"
	columns_to_standardize = []
	predict=TransformPredict(
		dataset_path,
		columns_to_standardize=columns_to_standardize)
	df = pd.read_csv(dataset_path)
	print(df)




if __name__ == '__main__':
	test()

