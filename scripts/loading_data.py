import pandas as pd
import numpy as np

class LoadData(object):

	def firstLoadData(self,PATH:None,Type:None):
		if PATH:
			data_file=pd.read_csv(PATH, sep=';',encoding='latin-1')
			data_file['TYPE'] = Type
		else:
			data_file=pd.read_csv("./datas/AltaStadistics.csv", sep=';',encoding='latin-1')
		return data_file

	def showInformation(self,data_file):
		print("INFO: ",data_file.info())
		print("HEAD: ",data_file.head())
		print("TAIL: ",data_file.tail())
		print("E.G: ",data_file.sample(5))
		print("DESC: ",data_file.describe())
		pd.isnull(data_file)

	def preprocess_data(self,data_file):
		"""Preprocesamiento del conjunto de datos."""
		df = data_file.reset_index()
		date = pd.to_datetime(df['FECHA'])
		df = df.drop('AÑO', axis=1)
		df.insert(0, 'Mes', date.dt.month)
		df.insert(1, 'Dia', date.dt.day)
		df.insert(1, 'Año', date.dt.year)
		df = df.drop('FECHA', axis=1)

		new_column_order = ['Dia', 'Mes', 'Año', 'SUSCEP', 'DETLLUVIA', 'DETSISMO', 'AMELLUVIA', 'AMESISMO','TYPE']
		data = df.reindex(columns=new_column_order)
		return data

  # Guardamos en un archivo csv, los datos de un DataFrame en un path especifico.
	def save_data(self,path,df):
		np.savetxt(path, df.as_matrix(), delimiter=',')

	def beginLoadData(self):
		loadData=LoadData();
		dataFrameList={}
		case="data"
		save_file_path = 'datasets/'+case+'production.csv'
		data_file_1=loadData.firstLoadData('./datas/AltaStadistics.csv',1)
		data_file_2=loadData.firstLoadData('./datas/OtrosStadistics.csv',0)
		data_file = data_file_1.append(data_file_2, ignore_index=True)
		# loadData.showInformation(wines)
		data_proccess=loadData.preprocess_data(data_file)

		# show Information
		loadData.showInformation(data_proccess)

		loadData.save_data(save_file_path,data_proccess)
		dataFrameList[case]=data_proccess

		print("\t ***   Finalized  data load    ***\n")

		return dataFrameList

def main():
	loadDataApi=LoadData()
	dataFrameList=loadDataApi.beginLoadData()

if __name__ == '__main__':
	main()